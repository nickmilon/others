"""others auyb"""
__author__ = '@nickmilon'


import argparse
from mongoUtils.client import muClient
from Hellas.Sparta import DotDot
from mongoUtils import importsExports as mex
from pymongo.errors import DuplicateKeyError, ServerSelectionTimeoutError
from bson.objectid import ObjectId
from copy import copy
from datetime import datetime
from pprint import pprint

try:
    import dstk
    from Hellas.Delphi import auto_retry # we only need it for dstk work
except ImportError:
    print("can't import dstk geotag will not work")
    pass


class AYUB(object):
    def __init__(self, args):
        self.clm = muClient(args.mongo, document_class=DotDot, w="1")
        self.clt = self._mongo_local() or self.clm
        self.args = args
        self.coll_tmp = self.clt.db.coll_tmp
        self.coll_normalize = self.clt.db.coll_normalize
        self.coll_duplicates = self.clt.db.coll_duplicates
        self.coll_place = self.clm.db.Place
        
    def _mongo_local(self):
        try:
            cll = muClient("mongodb://localhost:27017/databasefinal",  serverSelectionTimeoutMS=500, document_class=DotDot, w="1")
            cll.db.collection_names()
            return cll
        except ServerSelectionTimeoutError:
            return False

    def _proceed(self):
        print("arguments", self.args)
        print('coll_tmp:', self.coll_tmp)
        print('coll_normalize:', self.coll_normalize)
        try:
            input = raw_input
        except NameError:
            pass  # python 3
        res = input("proceed Y/N ? :") 
        if res != 'Y':
            print ("interrupted " * 5)
            return False
        return True

    def populate(self):
        doc_sample = self.coll_place.find_one({'title': 'VapourQueen'})
        if doc_sample is None:
            raise KeyError('doc_sample is None VapourQueen does not exist')
        _created_at = datetime.utcnow()
        rt = DotDot({'total': self.coll_normalize.count(), 'populated': 0})
        print ('progress ', rt)
        f = self.coll_normalize.find(no_cursor_timeout=True)
        for xls_doc in f: 
            doc = copy(doc_sample)
            doc.title = xls_doc['Shop_Name']
            doc.canonical = doc.title.lower()
            doc.address = xls_doc['address']
            doc.website = xls_doc['Website']
            doc._created_at = _created_at
            doc._updated_at = _created_at
            doc.phone = xls_doc.Phone
            doc.description = ''
            # oid = str(ObjectId())
            doc['_id'] = xls_doc['_id']
            # doc['_id'] = "Z" + oid[4:10] + oid[-3:] 
            geotag = xls_doc.get('geotag')
            location = [float(geotag['longitude']), float(geotag['latitude'])] if geotag else [-30, 30]
            doc.location = location
            del doc['ratingCount']
            del doc['ratingTotal']
            self.coll_place.insert_one(doc)
            rt.populated += 1
            if rt.populated % 500 == 0:
                print ('progress ', rt)
        f.close()

    def finalize (self):
        self.clt.db.drop_collection(self.coll_tmp.name)
        self.clt.db.drop_collection(self.coll_normalize.name)
        self.clt.db.drop_collection(self.coll_tmp.name)
        self.clt.db.drop_collection(self.coll_duplicates.name)

    def fetch_from_xls(self):
        fields_proper = ['Address_1', 'Address_2', 'Zip/Postal_Code', 'City', 'State/Province', 'Country' ,'Phone']

        def fields_set_proper():
            for fld in fields_proper:
                if isinstance(doc[fld], (float, int)):
                    doc[fld] = str(int(doc[fld]))
                else:
                    doc[fld] = doc[fld].encode('utf-8')
                doc[fld] = doc[fld].strip()

        self.finalize()
        self.clt.db.drop_collection(self.coll_tmp.name)
        self.clt.db.drop_collection(self.coll_normalize.name) 
        rt = mex.ImportXls(self.args.wb_path, 0, self.clt.db, self.coll_tmp.name)()
        print ("data imported to temporary collection")

        self.coll_normalize.drop_index('NM1')
        self.coll_normalize.create_index([('Shop_Name', 1), ('address', 1)], name='NM1', unique=True, background=False)
        rt = DotDot({'total': 0, 'duplicates': 0, 'inserted': 0})
        f = self.coll_tmp.find(sort=[('Website', -1), ('Phone', -1), ('Email', -1)], no_cursor_timeout=True)
        for doc in f:
            rt.total += 1
            oid = str(ObjectId())
            doc['_id'] = "Z" + oid[4:10] + oid[-3:]
            fields_set_proper()
            doc.address = "{}, {}, {}, {}, {}, {}".format(doc['Address_1'], doc['Address_2'], doc['Zip/Postal_Code'],
                                                          doc['City'],  doc['State/Province'], doc['Country'].encode('utf-8'))
            doc.Shop_Name = doc.Shop_Name.strip()
            try:
                self.coll_normalize.insert_one(doc)
                rt.inserted += 1
            except DuplicateKeyError:
                self.coll_duplicates.insert_one(doc)
                rt.duplicates += 1
            if rt.total % 1000 == 0:
                print ('inserting', rt)
        f.close()
        print ('fetch results', rt)

    def geotag(self):
        @auto_retry((ValueError, IOError))
        def auto_retry_geo(addr_str):
            return dstk_inst.street2coordinates(addr_str)

        dstk_inst = dstk.DSTK()
        f = self.coll_normalize.find({'geotag': {'$exists': False}}, no_cursor_timeout=True)
        rt = DotDot({'total': self.coll_normalize.count(), 'batch': f.count(), 'current': 0, 'geotaged': 0})
        print(rt)
        for doc in f:
            rt.current +=1
            address = doc['address'].replace(", ,", ",").replace(",,", ",")
            geo = auto_retry_geo(address)
            geo_tag = geo[address]
            if geo_tag is not None:
                rt.geotaged +=1
            self.coll_normalize.update_one({'_id': doc['_id']}, {'$set': {'geotag': geo_tag}})
            if rt.current % 200 == 0:
                print(rt)
        f.close()

    def duplicates(self): 
        path_out = self.args.wb_path.split(".")[0] + "_duplicates.csv"
        print("exporting duplicates to:" + path_out)
        fl = open(path_out, 'w')
        for i in self.coll_duplicates.find(sort=[('Shop_Name', 1), ('address', 1)]):
            s = "{} \t {}\n".format(i['Shop_Name'].encode('utf-8'), i['address'].encode('utf-8'))
            fl.write(s)
        fl.close()

def main():
        args = parse_args()
        ayub = AYUB(args) 
        if ayub._proceed() is True:
            print ("please wait until you see  'END' printed here")
            dowhat = args.dowhat 
            if dowhat in ['geotag']:
                ayub.geotag()
            if dowhat in ['fetch', 'all']:
                ayub.fetch_from_xls()
            if dowhat in ['populate', 'all']:
                ayub.populate()
            if dowhat in ['duplicates', 'all']:
                ayub.duplicates()
            if dowhat in ['finalize']:
                ayub.finalize()
            print (dowhat, ' finished', ' END ' * 10)


def parse_args():
    parser = argparse.ArgumentParser(description="Populate from Excel")
    parser.add_argument('-mongo',    default="mongodb://localhost:27017", type=str,
                        help='mongoDB connection string (see: https://docs.mongodb.com/manual/reference/connection-string/)')
    parser.add_argument('-dowhat', choices=['none', 'fetch', 'populate', 'finalize', 'geotag', 'all', 'duplicates'], default='all', 
                        help="what to do (geotag will not work without dsk) deafaults to all" )

    parser.add_argument("-wb_path", type=str,help='your workbook path name')

    return parser.parse_args()


if __name__ == "__main__":
    main() 