======
others
======

| libraries snippets and scripts developed as contracted work
  based mainly on `pymongo <http://api.mongodb.org/python/current/>`__
   

____

.. Note::
  - `git repository <https://bitbucket.org/nickmilon/others>`_
  - for any bugs/suggestions feel free to issue a ticket in bitbuckets `issues <https://bitbucket.org/nickmilon/others/issues>`_ 
    or send me a twitter dm @nickmilon

____

:Installation:
   - install pip if you have not done already `see here for how to install pip <https://pip.pypa.io/en/stable/installing/>`__ 
   - optionaly install and use virtualenv `see here for how to install and use virtualenv <https://virtualenv.pypa.io/en/stable/installation/>`__ 
   - ``pip install git+https://bitbucket.com/nickmilon/others`` 
   
____

:Dependencies:
   - `pymongo <http://api.mongodb.org/python/current/>`__ (installed automatically by setup)
   - `Hellas <http://miloncdn.appspot.com/docs/Hellas/index.html>`_ (installed automatically by setup) a lightweight python utilities library of mine
   - `mongoUtils <http://miloncdn.appspot.com/docs/mongoUtils/index.html>`_ (installed automatically by setup) a pymongo extentions lib of mine
   - `xlrd library <https://pypi.python.org/pypi/xlrd>`_  (installed automatically by setup)  

____

:Usage ayub: 
   >>> python -m others.ayub -h   (help screen explains all options)
   >>> python -m others.ayub -mongo "your mongodb connection uri" -wb_path "path to your .xlsx" -dowhat all  
  
____


 
